"use strict"

const drawButton = document.getElementById('draw-button');
const body = document.querySelector('body');
const circlesContainer = document.createElement('div');
body.appendChild(circlesContainer);
let diameterInput;

function drawCircles() {
  const diameter = diameterInput.value;
  circlesContainer.innerHTML = '';
  for (let i = 0; i < 100; i++) {
    const circle = document.createElement('div');
    circle.classList.add('circle');
    circle.style.backgroundColor = getRandomColor();
    circlesContainer.appendChild(circle);
  }
  setCircleClickHandlers();
}

function setCircleClickHandlers() {
  const circles = document.querySelectorAll('.circle');
  for (let i = 0; i < circles.length; i++) {
    const circle = circles[i];
    circle.addEventListener('click', () => {
      circle.remove();
      fillGap();
      });
    }
}

function fillGap() {
  const circles = document.querySelectorAll('.circle');
  for (let i = 0; i < circles.length; i++) {
    const circle = circles[i];
    if (i % 10 === 0 && i !== 0) {
      circle.style.clear = 'left';
    }
  }
}

function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

drawButton.addEventListener('click', () => {
  diameterInput = document.createElement('input');
  diameterInput.type = 'number';
  diameterInput.placeholder = 'Введіть діаметр кола';
  body.appendChild(diameterInput);

  const submitButton = document.createElement('button');
  submitButton.textContent = 'Намалювати';
  body.appendChild(submitButton);

  submitButton.addEventListener('click', drawCircles);
});